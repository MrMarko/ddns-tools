<?php
include_once 'ddns.php';

$ddns = new ddns();

$save = $ddns->save($_POST);

$ddns->log([
    '类型' => '修改配置',
    '结果' => $save ? '成功' : '失败',
]);
// 启动
setRun(2);

// 如果关闭则停止运行
if ($_POST['open'] != 'on') {
    setRun(0);
}

// 保存后跳转回首页
//header('location:/');


function setRun($pid)
{
    return file_put_contents('run.pid', $pid);
}