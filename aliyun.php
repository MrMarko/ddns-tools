<?php
require_once 'Common.php';

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;

class aliyun extends Common
{
    public function __construct($domain, $ip)
    {
        AlibabaCloud::accessKeyClient(self::$config['id'], self::$config['secret'])
                    ->regionId('cn-hangzhou')
                    ->asDefaultClient();

        $this->set($domain, $ip);

        $this->getRR($domain);
    }

    public function update()
    {
        $list = $this->getDomainList();

        if ($this->error) {
            return false;
        }

        $result = false;

        // 查看列表里是否有 如果没有则需要创建
        if ($list) {
            foreach ($list as $value) {
                if ($value['RR'] == $this->rr) {
                    if ($value['Value'] == $this->ip) {
                        $this->error = '暂无任何修改';

                        break;
                    }
                    // 找到相同的域名的RecordId 并更新
                    $result = $this->updateDomain($value['RecordId']);
                }
            }
        } else {
            // 添加域名
            $result = $this->addDomain();
        }

        return $result;
    }

    /**
     * 更新域名
     *
     * @param $recordid
     * @return $this|bool
     */
    public function updateDomain($recordid)
    {
        try {
            $result = AlibabaCloud::rpc()
                                  ->product('Alidns')
                                  ->version('2015-01-09')
                                  ->action('UpdateDomainRecord')
                                  ->method('POST')
                                  ->host('alidns.aliyuncs.com')
                                  ->options([
                                      'query' => [
                                          'RegionId' => "cn-hangzhou",
                                          'RR'       => $this->rr,
                                          'Type'     => "A",
                                          'Value'    => $this->ip,
                                          'RecordId' => $recordid
                                      ],
                                  ])
                                  ->request();
            return true;
        } catch (ClientException $e) {
            $this->error = $e->getMessage();
        } catch (ServerException $e) {
            $this->error = $e->getMessage();
        }

        return false;
    }

    /**
     * 解析记录
     *
     * @return bool
     */
    public function getDomainList()
    {
        try {
            $result = AlibabaCloud::rpc()
                                  ->product('Alidns')
                // ->scheme('https') // https | http
                                  ->version('2015-01-09')
                                  ->action('DescribeDomainRecords')
                                  ->method('POST')
                                  ->host('alidns.aliyuncs.com')
                                  ->options([
                                      'query' => [
                                          'RegionId'   => "cn-hangzhou",
                                          'DomainName' => $this->domain,
                                          'RRKeyWord'  => $this->rr,
                                          'SearchMode' => "ADVANCED",
                                      ],
                                  ])
                                  ->request();

            $list = $result->toArray()['DomainRecords']['Record'];

            return $list;
        } catch (ClientException $e) {
            $this->error = $e->getMessage();
        } catch (ServerException $e) {
            $this->error = $e->getMessage();
        }

        return false;
    }

    /**
     * 添加域名
     *
     * @return $this|bool
     */
    public function addDomain()
    {
        try {
            $result = AlibabaCloud::rpc()
                                  ->product('Alidns')
                // ->scheme('https') // https | http
                                  ->version('2015-01-09')
                                  ->action('AddDomainRecord')
                                  ->method('POST')
                                  ->host('alidns.aliyuncs.com')
                                  ->options([
                                      'query' => [
                                          'RegionId'   => "cn-hangzhou",
                                          'DomainName' => $this->domain,
                                          'RR'         => $this->rr,
                                          'Type'       => "A",
                                          'Value'      => $this->ip,
                                      ],
                                  ])
                                  ->request();

            return true;
        } catch (ClientException $e) {
            $this->error = $e->getMessage();
        } catch (ServerException $e) {
            $this->error = $e->getMessage();
        }

        return false;
    }
}