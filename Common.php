<?php
class Common
{
    protected static $config;

    protected $status = '成功';

    protected $domain = '';

    protected $rr = '';

    protected $ip = '';

    protected $full_domain = '';

    protected $error = '';

    /**
     * 记录日志
     *
     * @param $content
     * @return bool
     */
    public function log($content)
    {
        date_default_timezone_set('PRC');

        $log = json_decode(file_get_contents('log.json'), true);

        array_unshift($log, [
            'date'    => date('Y-m-d H:i:s'),
            'content' => $content
        ]);

        file_put_contents('log.json', json_encode($log));

        return true;
    }

    public function set($domain, $ip)
    {
        $this->ip = $ip;

        $this->full_domain = $domain;
    }

    /**
     * 获取域名前缀
     *
     * @param $domain
     * @return string
     */
    protected function getRR($domain)
    {
        $rr = explode('.', $domain);

        $d = [];

        array_unshift($d, array_pop($rr));
        array_unshift($d, array_pop($rr));

        $this->domain = join('.', $d);

        $this->rr = join('.', $rr) ?: '@';

        return $this;
    }
}