<?php
include_once 'ddns.php';

$ddns = new ddns();

$config = json_decode(file_get_contents('config.json'), true);

$sleep_time = $config['check_time'] * 60;

while (true) {
    $return = pid(file_get_contents('run.pid'));

    if ($return) {
        $new_ip = $ddns->getIp();

        // 如果两个IP不一致则代表IP更换了 则需要更新阿里云
        if ($new_ip !== false && $config['ip'] != $new_ip) {
            $config['ip'] = $new_ip;

            // 修改IP地址
            $ddns->save($config);

            foreach ($config['domain'] as $v) {
                $ddns->change($v, $new_ip);
            }
        }

        sleep($sleep_time);
    } else {
        sleep(5);
    }
}


function pid($pid)
{
    global $config, $sleep_time;

    switch ($pid) {
        case 0:
            return false;
            break;
        case 1:
            return true;
            break;
        // 2为重启 则需重新获取配置
        case 2:
            $config = json_decode(file_get_contents('config.json'), true);

            file_put_contents('run.pid', 1);

            return true;
            break;
    }

    return true;
}