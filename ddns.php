<?php
require_once './vendor/autoload.php';
require_once 'Common.php';
require_once 'aliyun.php';

class ddns extends Common
{
    public function __construct()
    {
        self::$config = json_decode(file_get_contents('config.json'), true);
    }

    public function change($domain, $ip)
    {
        // 如果没有打开开关则不执行任何操作
        if (!$this->checkOpen()) {
            return true;
        }

        $this->set($domain, $ip);

        switch (self::$config['server']) {
            case 'aliyun':
                $server = new aliyun($domain, $ip);
                break;
            default:
                return false;
                break;
        }

        // 更新域名
        $update = $server->update();

        $log = [
            '类型' => 'DNS更新',
            'IP' => $this->ip,
            '域名' => $this->full_domain,
        ];

        if ($update) {
            $log['结果'] = '更新成功';

            $this->reset('成功');

            $this->log($log);
        } else {
            $log['结果'] = '更新失败:'.$server->error;

            $this->reset('失败', $server->error);

            $this->log($log);
        }

        // 执行推送
        $this->send();
    }

    /**
     * webhook推送
     */
    public function send()
    {
        $send_url = self::$config['send_url'];

        $var = [
            'ip'     => $this->ip ?: $this->getIp(),
            'status' => $this->status,
            'domain' => $this->full_domain ?: @self::$config['domain'][0],
            'error' => str_replace("\"", "'", $this->error)
        ];

        // 推送地址不为空 才使用推送
        if (!empty($send_url)) {
            $content = [
                'post'  => true,
                'query' => self::$config['send_content'],
            ];

            foreach ($var as $k => $v) {
                $content['query'] = str_replace('{$' . $k . '}', $v, $content['query']);
                $send_url = str_replace('{$' . $k . '}', $v, $send_url);
            }

            if (!empty(json_decode(self::$config['send_content'], true))) {
                $content['header'] = [
                    'Content-Type: application/json'
                ];
            }

            $send = $this->curl($send_url, $content);

            if (!$send) {
                $log = [
                    '类型' => '推送失败',
                    '错误内容' => $send,
                ];

                $this->log($log);

                return '推送失败';
            }

            return $send;
        }

        return true;
    }

    /**
     * 获取ip
     *
     * @return string
     */
    public function getIp()
    {
        $ip = file_get_contents('http://ip.cip.cc/');

        $ip = trim($ip);

        if(!filter_var($ip, FILTER_VALIDATE_IP)) {
            return false;
        }

        return $ip;
    }

    public function curl($url, $option = [])
    {
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 500);

        if (@$option['post']) {
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $option['query']);
        }

        if (@$option['header']) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $option['header']);
        }
        // 为保证第三方服务器与微信服务器之间数据传输的安全性，所有微信接口采用https方式调用，必须使用下面2行代码打开ssl安全校验。
        // 如果在部署过程中代码在此处验证失败，请到 http://curl.haxx.se/ca/cacert.pem 下载新的证书判别文件。
        // curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

        $res = curl_exec($curl);

        curl_close($curl);

        return $res;
    }

    /**
     * 保存配置
     *
     * @param $content
     * @return bool|int
     */
    public function save($content)
    {
        foreach ($content['domain'] as $k => $v) {
            if (empty($v)) {
                unset($content['domain'][$k]);
            }
        }

        // 写入配置
        $save = file_put_contents('config.json', json_encode($content));

        return $save;
    }

    /**
     * 检测是否启用开关
     *
     * @return bool
     */
    public function checkOpen()
    {
        return self::$config['open'] == 'on' ? true : false;
    }

    public function reset($status, $error = '')
    {
        $this->status = $status;

        $this->error = $error;

        return $this;
    }
}