# DDNS-Tools

#### 介绍
DDNS工具 实时监听并解析 暂时只支持阿里云 后期会开放更多服务商

之前看过几个DDNS的工具，但是感觉界面不是特别好看，就想着自己开发一个，也不难。废话不多说，直接上使用教程吧



- 首页（可使用立即更新马上更新当前域名的IP）

![2021010415362516097457851609745785541ojJBMO](https://tva1.sinaimg.cn/large/0081Kckwgy1gmbpauhtnnj314m0bnjs1.jpg)

- DNS服务商

![2021010415371616097458361609745836207ftI5tG](https://tva1.sinaimg.cn/large/0081Kckwgy1gmbpbmcj2tj31570bkwfa.jpg)

- 域名批量设置

![2021010415372716097458471609745847577HjLXSh](https://tva1.sinaimg.cn/large/0081Kckwgy1gmbpbvaojgj315i09gdgf.jpg)

- Webhook消息推送（可支持钉钉）

![2021010415373916097458591609745859273xX0AT7](https://tva1.sinaimg.cn/large/0081Kckwgy1gmbpc1j0nkj312h0kv40d.jpg)

- 系统日志（修改配置和更新都会有日志）

![20210104153816160974589616097458966677i0LuA](https://tva1.sinaimg.cn/large/0081Kckwgy1gmbpcoq9igj313d09vt90.jpg)



1. 搭建好PHP环境即可下载代码 并添加站点即可访问该网站进行设置(PHP环境自行搭建)

2. 设置好以上的参数 使用命令行执行即可 如果需要后台执行 也可安装Screen 放在后台执行 具体Screen的操作请百度

```php
php run.php
```

3. 当运行后再修改配置也是会生效的 无需重启run.php文件

   