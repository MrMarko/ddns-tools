<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>PDNS</title>
    <link rel="stylesheet" href="./layui/css/layui.css">
</head>
<style type="text/css">
    .layui-form-label {
        width: 120px;
    }

    .layui-input-block {
        margin-left: 150px;
    }
</style>
<body>
<?php
$default = json_decode(file_get_contents('config.json'), true);

$log = json_decode(file_get_contents('log.json'), true);
?>

<form class="layui-form"  action="post.php" method="post">
    <input type="hidden" value="<?=$default['ip']?>" name="ip">
    <!-- 你的HTML代码 -->
    <div class="layui-container">
        <div class="layui-row">
            <div class="layui-col-md12">
                <div class="layui-tab">
                    <ul class="layui-tab-title">
                        <li class="layui-this">首页</li>
                        <li>DNS服务商</li>
                        <li>域名设置</li>
                        <li>消息推送</li>
                        <li>日志</li>
                    </ul>

                    <div class="layui-tab-content">
                        <!--                    首页-->
                        <div class="layui-tab-item layui-show">
                            <blockquote class="layui-elem-quote">DDNS自动化工具 <span style="float: right"> 作者：冷猿科技</span></blockquote>
                            <div class="layui-form-item">
                                <label class="layui-form-label">启用</label>
                                <div class="layui-input-block">
                                    <input type="checkbox" <?= $default['open'] == 'on' ? 'checked' : '' ?> name="open" lay-skin="switch">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">检测间隔时长/分钟</label>
                                <div class="layui-input-block">
                                    <input type="text" name="check_time" required lay-verify="required" placeholder="请输入间隔时长" value="<?= $default['check_time'] ?>" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">DDNS更新</label>
                                <div class="layui-input-block">
                                    <button class="layui-btn layui-btn-sm" lay-submit lay-filter="update_ddns">立即更新</button>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                                </div>
                            </div>
                        </div>
                        <!--                     DNS服务商-->
                        <div class="layui-tab-item">
                            <blockquote class="layui-elem-quote">
                                阿里云获取地址：<a href="https://ram.console.aliyun.com/manage/ak" target="_blank">https://ram.console.aliyun.com/manage/ak</a>
                            </blockquote>
                            <div class="layui-form-item">
                                <label class="layui-form-label">服务商</label>
                                <div class="layui-input-block">
                                    <input type="radio" name="server" <?= $default['server'] == 'aliyun' ? 'checked' : '' ?> value="aliyun" title="阿里云">
<!--                                    <input type="radio" name="server" --><?//= $default['server'] == 'tenxunyun' ? 'checked' : '' ?><!-- value="tenxunyun" title="腾讯云">-->
<!--                                    <input type="radio" name="server" --><?//= $default['server'] == 'huaweiyun' ? 'checked' : '' ?><!-- value="huaweiyun" title="华为云">-->
                                </div>
                            </div>

                            <div class="layui-form-item">
                                    <label class="layui-form-label">AccessKey ID</label>
                                <div class="layui-input-block">
                                    <input type="text" name="id" value="<?= $default['id'] ?>" placeholder="请输入AccessKey ID" autocomplete="off" class="layui-input">
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <label class="layui-form-label">AccessKey Secret</label>
                                <div class="layui-input-block">
                                    <input type="text" name="secret" value="<?= $default['secret'] ?>" placeholder="请输入AccessKey Secret" autocomplete="off" class="layui-input">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                                </div>
                            </div>
                        </div>
                        <!--                      域名设置-->
                        <div class="layui-tab-item">
                            <blockquote class="layui-elem-quote">
                                Tips:可添加多个域名 当IP改变时会同时更新多个域名，如果该域名不存在则会新增一个新的域名记录
                            </blockquote>
                            <div class="layui-form-item">
                                <label class="layui-form-label">域名</label>
                                <div class="layui-input-block" id="domain">
                                    <?php
                                    if (empty($default['domain'])) {
                                    ?>
                                    <input type="text" name="domain[]" placeholder="请输入域名" autocomplete="off" style="margin-bottom: 5px;" class="layui-input">
                                    <?php
                                    }
                                    foreach ($default['domain'] as $v) {
                                        if (!empty($v)) {
                                            ?>
                                            <input type="text" name="domain[]" value="<?= $v ?>" placeholder="请输入域名" autocomplete="off" style="margin-bottom: 5px;" class="layui-input">
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div style="text-align: center">
                                    <button type="button" class="layui-btn" id="add">
                                        <i class="layui-icon">&#xe608;</i>
                                    </button>
                                </div>
                            </div>


                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                                </div>
                            </div>
                        </div>
                        <!--                      消息推送-->
                        <div class="layui-tab-item">
                            <blockquote class="layui-elem-quote">推送变量：
                                <br>&nbsp;&nbsp;&nbsp;&nbsp;{$ip}为最新的IP
                                <br>&nbsp;&nbsp;&nbsp;&nbsp;{$status}为更新结果
                                <br>&nbsp;&nbsp;&nbsp;&nbsp;{$domain}为更新的域名
                                <br>&nbsp;&nbsp;&nbsp;&nbsp;{$error}为更新失败的错误内容，只有失败才会有内容
                                <br>推送示例：
                                <br>&nbsp;&nbsp;&nbsp;&nbsp;你的IP地址已变为{$ip}，更新结果: {$status}，域名 :{$domain}，失败原因：{$error}
                                <br>Tips：
                                <br>&nbsp;&nbsp;&nbsp;&nbsp;只填推送地址的话使用GET推送 填写了推送内容则使用POST推送 POST内容发送类型为application/json

                            </blockquote>

                            <div class="layui-form-item">
                                <label class="layui-form-label">推送地址</label>
                                <div class="layui-input-block">
                                    <input type="text" name="send_url" value="<?=$default['send_url']?>" placeholder="请输入推送地址" autocomplete="off" class="layui-input">
                                </div>
                            </div>

                            <div class="layui-form-item layui-form-text">
                                <label class="layui-form-label">推送内容</label>
                                <div class="layui-input-block">
                                    <textarea name="send_content" rows="15" placeholder="请输入推送内容" class="layui-textarea"><?=$default['send_content']?></textarea>
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                                    <button class="layui-btn layui-btn-normal" id="test_send">测试推送</button>
                                    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                                </div>
                            </div>
                        </div>
                        <!--                      系统日志 -->
                        <div class="layui-tab-item" style="height:800px;overflow: scroll;">
                            <blockquote class="layui-elem-quote">
                                <ul class="layui-timeline">
                                    <?php
                                    if (empty($log)) {
                                        ?>
                                        <li class="layui-timeline-item">
                                            <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                                            <div class="layui-timeline-content layui-text">
                                                <h3 class="layui-timeline-title">暂无日志</h3>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    foreach ($log as $v) {

                                        ?>
                                        <li class="layui-timeline-item">
                                            <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                                            <div class="layui-timeline-content layui-text">
                                                <h3 class="layui-timeline-title"><?=$v['date']?></h3>
                                                <p>
                                                    <?php
                                                        foreach ($v['content'] as $key => $value) {
                                                            echo "{$key}：{$value}<br>";
                                                        }
                                                    ?>
                                                </p>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    ?>

                                    <li class="layui-timeline-item">
                                        <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                                        <div class="layui-timeline-content layui-text">
                                            <div class="layui-timeline-title">End</div>
                                        </div>
                                    </li>
                                </ul>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>


<script src="./layui/layui.js"></script>
<script>
    //一般直接写在一个js文件中
    layui.use(['layer', 'form', 'element', 'code'], function () {
        var layer = layui.layer
            , form = layui.form
            , element = layui.element
            , code = layui.code
            ,$ = layui.$;
        // layer.msg('Hello World');

        $('#test_send').click(function() {
            //loading层
            var index = layer.load(1, {
                shade: [0.1,'#fff'] //0.1透明度的白色背景
            });

            $.ajax({
                //请求方式
                type : "POST",
                //请求地址
                url : "/test_send.php",
                //请求成功
                success : function(result) {
                    layer.alert(result, {
                        skin: 'layui-layer-molv' //样式类名
                        ,closeBtn: 0
                        ,title:'推送结果'
                    });
                },
                //请求失败，包含具体的错误信息
                error : function(e){
                    layer.msg('请求失败：'.e);
                }
            });

            layer.close(index);

            return false;
        });


        form.on('submit(formDemo)', function(data){
            //loading层
            var index = layer.load(1, {
                shade: [0.1,'#fff'] //0.1透明度的白色背景
            });

            $.ajax({
                //请求方式
                type : "POST",
                //请求地址
                url : "/post.php",
                // 请求内容
                data: data.field,
                //请求成功
                success : function(result) {
                    layer.msg('保存成功');

                    layer.close(index);
                },
                //请求失败，包含具体的错误信息
                error : function(e){
                    layer.msg('请求失败：'.e);

                    layer.close(index);
                }
            });


            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });

        $('#add').click(function(){
            $('#domain').append('<input type="text" name="domain[]" value="" placeholder="请输入域名" autocomplete="off" style="margin-bottom: 5px;" class="layui-input">');
        })

        form.on('submit(update_ddns)', function(data){
            //loading层
            var index = layer.load(1, {
                shade: [0.1,'#fff'] //0.1透明度的白色背景
            });

            $.ajax({
                //请求方式
                type : "get",
                //请求地址
                url : "/test_run.php",
                //请求成功
                success : function(result) {
                    layer.msg('更新成功');

                    layer.close(index);
                },
                //请求失败，包含具体的错误信息
                error : function(e){
                    layer.msg('更新失败：'.e);

                    layer.close(index);
                }
            });

            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });
    });


</script>
</body>
</html>