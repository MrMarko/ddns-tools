<?php
include_once 'ddns.php';

$ddns = new ddns();

$config = json_decode(file_get_contents('config.json'), true);

$new_ip = $ddns->getIp();

$config['ip'] = $new_ip;

// 修改IP地址
$ddns->save($config);

foreach ($config['domain'] as $v) {
    $ddns->change($v, $new_ip);
}