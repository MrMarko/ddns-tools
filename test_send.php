<?php
include_once 'ddns.php';

$ddns = new ddns();

$config = json_decode(file_get_contents('config.json'), true);

$return = $ddns->send();

$ddns->log([
    '类型' => '测试推送',
    '结果' => $return,
]);

echo $return;

